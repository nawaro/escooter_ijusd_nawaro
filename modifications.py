from full_functions import Modify
import pandas as pd
import os

filesuffix = 'allveturilo'

m = Modify(trips=pd.read_pickle(os.path.join('..', 'out', 'all_trips_mod_{}.pickle'.format(filesuffix))),
           filesuffix=filesuffix)
m.access('hive', 'veturilo', os.path.join('..', 'out', 'cross_counts_veturilo_{}.pickle'.format(filesuffix)),
         None)
m.access('hive', 'random', os.path.join('..', 'out', 'cross_counts_random_{}.pickle'.format(filesuffix)),
         'random_cross', random_access=True)
m.save('access')
