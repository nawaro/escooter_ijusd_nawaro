# coordinates
from transformers import tt, ttr

# data processing
import pandas as pd
import numpy as np
np.random.seed(123)
import random
import scipy
import os
import math

# visualization
import matplotlib
# garbage
import gc

# functions
from full_functions import *

filesuffix = 'total'

# plot settings
font = {'size': 22}
matplotlib.rc('font', **font)
sns.set_style('dark')
#img_dir = os.path.join('/', 'home', 'lukasz', 'phd', 'scooter_article', 'img')
img_dir = os.path.join('..', 'out', 'img')
print(img_dir)

# read data
trips = pd.read_pickle('../out/all_trips_access_{}.pickle'.format(filesuffix))
stays = {'hive': pd.read_pickle('../out/hive_stays_{}.pickle'.format(filesuffix))}
relocations = {'hive': pd.read_pickle('../out/hive_relocations_{}.pickle'.format(filesuffix))}

for k, v in trips.items():
    print(k, v.shape)
print('stays', stays['hive'].shape)

for k, v in trips.items():
    for se in ['start', 'end']:
        trips[k]['{}_periphery'.format(se)] = ~v['{}_central'.format(se)]

#trips['hive']['start_place_veturilo_coord'] = trips[
#'hive']['start_place_veturilo'].map(
#    trips['veturilo'][
#    ['end_place_number', 'end_coords']].set_index(
#        'end_place_number').to_dict()['end_coords']).apply(
#lambda x: [float(i) for i in x.split(', ')])


def flatten(x):
    return [a for b in x for a in b]


def get_coords(x):
    try:
        return [[j.coords[0] for j in i.geoms] for i in x]
    except Exception as ex:
        return [([i.coords[0]] if type(i) == Point else [j.coords[0] for j in i.geoms]) for i in x]


def jitter(x, r=1):
    angle = random.uniform(0, 2*math.pi)
    return x[0] + r*math.cos(angle), x[1] + r*math.sin(angle)


ant = Analysis(trips, stays, relocations, filesuffix, img_dir=img_dir)

### Trip lengths
print('\n---TRIP LENGTHS\n---')
chart_columns = ['duration', 'otp_length', 'speed']
for se in ['start', 'end']:
    print(se)
    for is_all in [False, True]:
        for col in chart_columns:
            ant.plot_kde(se, is_all, col)

for col in chart_columns:
    for k in ant.trips.keys():
        print(k, col, '\n', trips[k][col].describe(), '\n---')

print('\n---Kolmogorov-Smirnov\n---')

print('all Veturilo vs all Hive')
for col in chart_columns:
    print(col, 'all')
    print(ant.ks_test(['veturilo', 'hive'], [None, None], col))
    for se in ['start', 'end']:
        print(se, col)
        
        print('central Veturilo vs central Hive')
        print(ant.ks_test(['veturilo', 'hive'], [
            {'se': se, 'group': 'central', 'bool': True},
            {'se': se, 'group': 'central', 'bool': True}], col))
        
        print('central Veturilo vs all Hive')
        print(ant.ks_test(['veturilo', 'hive'], [
            {'se': se, 'group': 'central', 'bool': True},
            None], col))

        print('periphery Veturilo vs all Hive')
        print(ant.ks_test(['veturilo', 'hive'], [
            {'se': se, 'group': 'periphery', 'bool': True},
            None], col))

        print('central Veturilo vs periphery Veturilo')
        print(ant.ks_test(['veturilo', 'veturilo'], [
            {'se': se, 'group': 'central', 'bool': True},
            {'se': se, 'group': 'periphery', 'bool': True}], col))

### Access to shared bicycles
print('\n---ACCESS\n---')
print(trips['hive'].loc[trips['hive']['start_central'] == True, 'start_dist_to_veturilo'].describe())
print(trips['hive'].loc[trips['hive']['start_central'] == True, 'end_dist_to_veturilo'].describe())

ant.access('hive', 'random',
          filename=os.path.join('..', 'out', 'cross_counts_random_{}.pickle'.format(filesuffix)),
          column='chosen_random_point')
ant.access('hive', 'veturilo',
          filename=os.path.join('..', 'out', 'cross_counts_veturilo_{}.pickle'.format(filesuffix)),
          column=None)

print(trips['hive']['cross_counts_random'].describe())
print(trips['hive']['cross_counts_veturilo'].describe())

print('below mean')
print(trips['hive'].loc[trips['hive']['end_dist_to_veturilo'] < trips['hive']['end_dist_to_veturilo'].mean()]['cross_counts_random'].describe())
print(trips['hive'].loc[trips['hive']['end_dist_to_veturilo'] < trips['hive']['end_dist_to_veturilo'].mean()]['cross_counts_veturilo'].describe())

trips['hive']['angle_difference'] = np.min(
    [(trips['hive']['angle'] - trips['hive']['veturilo_angle']).values % 360,
     (trips['hive']['veturilo_angle'] - trips['hive']['angle']).values % 360],
    axis=0)

ant.plot_angles('hive', 'veturilo', 'median')
ant.plot_angles('hive', 'veturilo', None)

### Transportation hubs
hubs = {
    'politechnika': tt(21.014393, 52.217601),
    'swietokrzyska': tt(21.008441, 52.235249),
    'onz': tt(20.998240, 52.233104),
    'centrum': tt(21.011740, 52.229793),
    'daszynskiego': tt(21.014393, 52.217601),
    'zawiszy': tt(20.989125, 52.224853),
    'mirowska': tt(20.996079, 52.238329),
    'bristol': tt(21.015223, 52.242126),
    'wronia': tt(20.988685, 52.231442),
    'poznanska': tt(21.012853, 52.223501)
}
ant.relocation_map('hive')
ant.near_hubs('hive', hubs, 120)

### Public transport

for k in ant.trips.keys():
    for pt in ['metro', 'tram']:
        for se in ['start', 'end']:
            for group in ['central', 'periphery']:
                ant.percentages_groups(k, pt, group=group, se=se)

for k in ant.trips.keys():
    ant.transfers(k)
    ant.transfers(k, 5, 20)
    ant.transfers(k, 10, 20)
    ant.transfers(k, 5, 10)
