from transformers import tt, ttr
import pandas as pd
import numpy as np
from scipy import stats
from scipy.spatial import KDTree

import os
from datetime import datetime
import pickle
import requests
import json
import math
import random

# plot
import matplotlib.pyplot as plt
import seaborn as sns
import folium

# lines
from shapely.geometry import LineString
from shapely.geometry import Point, Polygon


class Transport:
    def __init__(self, trips, filesuffix):
        self.trips = trips
        self.filesuffix = filesuffix
        self.groups = ['central', 'periphery']

    @staticmethod
    def get_duration(v):
        parameters = 'fromPlace=' + str(v['start_lat']) + ',' + str(
            v['start_lon']) + '&toPlace=' + str(
            v['end_lat']) + ',' + str(
            v['end_lon']) + '&mode=BICYCLE&triangleSafetyFactor=0.5&triangleSlopeFactor=0.2&triangleTimeFactor=0.3'
        filename = '../out/routes_point/' + parameters + '.json'

        if os.path.exists(filename):
            trip_stations = json.load(open(filename, 'r'))
        else:
            route = requests.get('http://localhost:8585/otp/routers/default/plan?' + parameters)
            json.dump(route.json(), open(filename, 'w'))
            trip_stations = json.load(open(filename, 'r'))

        if trip_stations.get('error'):
            return {'duration': 0, 'length': 0}
        else:
            trip_stations = trip_stations['plan']['itineraries'][0]
            return {'duration': trip_stations['duration'],
                    'length': trip_stations['walkDistance']}

    @staticmethod
    def day_translate(weekday):
        return 23 + ((weekday - 3) % 7)

    def get_transit_alternative(self, v):
        date_row = pd.to_datetime(v['start_time'])
        day_of_month = self.day_translate(date_row.weekday())
        hour_string = date_row.strftime('%I:%M%p').lower()
        print(v['start_time'])
        print(date_row)
        print(day_of_month)
        print(hour_string)
        parameters = 'fromPlace=' + \
                     str(v['start_lat']) + ',' + str(v['start_lon']) + '&toPlace=' + str(v['end_lat']) + ',' + str(
            v['end_lon']) + '&mode=TRANSIT,WALK&arriveBy=false&maxWalkDistance=500&date=01-{}-2020&time={}'.format(
            day_of_month, hour_string.replace('+00:00', ''))
        filename = os.path.join('..', 'out', 'routes_public', parameters + '.json')

        if os.path.exists(filename):
            trip_stations = json.load(open(filename, 'r'))
        else:
            route = requests.get('http://localhost:8585/otp/routers/default/plan?' + parameters)
            json.dump(route.json(), open(filename, 'w'))
            trip_stations = json.load(open(filename, 'r'))

        if trip_stations.get('error'):
            return {'duration': 0, 'transit_duration': 0, 'waiting_duration': 0, 'transfers': 0, 'walking_length': 0}
        else:
            trip_stations = trip_stations['plan']['itineraries'][0]
            dr = {'duration': trip_stations['duration'],
                    'transit_duration': trip_stations['transitTime'],
                    'waiting_duration': trip_stations['waitingTime'],
                    'transfers': trip_stations['transfers'],
                    'walking_length': trip_stations['walkDistance']}
            print(dr, v['start_lat'], v['start_lon'], v['end_lat'], v['end_lon'])
            return dr

    @staticmethod
    def time_of_day(x):
        if x < 7:
            return 'night'
        if x < 12:
            return 'morning'
        if x < 15:
            return 'midday'
        if x < 20:
            return 'afternoon'
        return 'night'


class Modify(Transport):
    def __init__(self, trips, filesuffix):
        Transport.__init__(self, trips, filesuffix)

    def remove_roundtrips(self, provider, latlon=True, column_suffix=None):
        if latlon:
            self.trips[provider] = self.trips[provider].loc[
                (self.trips[provider]['start_lon'] != self.trips[provider]['end_lon']) &
                (self.trips[provider]['start_lat'] != self.trips[provider]['end_lat'])]
        if column_suffix is not None:
            self.trips[provider] = self.trips[provider].loc[
                self.trips[provider]['start_{}'.format(column_suffix)] != self.trips[provider][
                    'end_{}'.format(column_suffix)]
            ]

    def column_times(self, provider):
        self.trips[provider]['start_time_of_day'] = self.trips[provider]['start_time'].apply(lambda x: x.hour).map(self.time_of_day)

    def otp_values(self, provider):
        otp_data = self.trips[provider].apply(self.get_duration, axis=1)
        for d in ['duration', 'length']:
            self.trips[provider]['otp_{}'.format(d)] = otp_data.apply(lambda x: x[d])
        self.trips[provider]['otp_duration'] /= 60

    def otp_alternative(self, provider):
        otp_transit_data = self.trips[provider].apply(self.get_transit_alternative, axis=1)
        for d in otp_transit_data.iloc[0].keys():
            self.trips[provider]['transit_{}'.format(d)] = otp_transit_data.apply(lambda x: x[d])
            
    def km_coords(self, provider, se):
        new_coords = self.trips[provider].apply(lambda row: tt(row['{}_lon'.format(se)],
                                                   row['{}_lat'.format(se)]), axis=1)
        self.trips[provider]['{}_km_lon'.format(se)] = new_coords.apply(lambda x: x[0])
        self.trips[provider]['{}_km_lat'.format(se)] = new_coords.apply(lambda x: x[1])
        
    def centrality(self, provider, central_polygon=Polygon([[20.9738445, 52.208606],
                   [21.0094643, 52.2048978],
                   [21.0561562, 52.2177042],
                   [21.0552979, 52.2388384],
                   [21.042273, 52.2538747],
                   [21.0311365, 52.2619589],
                   [21.0209227, 52.2670543],
                   [20.9788227, 52.2644279],
                   [20.9597683, 52.2524495],
                   [20.9489536, 52.2371039],
                   [20.9557343, 52.2127609],
                   [20.9711838, 52.2090794]]
                  )):
        self.trips[provider]['end_central'] = self.trips[provider].apply(lambda row: central_polygon.contains(Point(
            float(row['end_lon']), float(row['end_lat']))), axis=1)
        self.trips[provider]['start_central'] = self.trips[provider].apply(lambda row: central_polygon.contains(Point(
            float(row['start_lon']), float(row['start_lat']))), axis=1)
            
    def remove_suspects(self, provider, max_duration_ratio=8/3, max_speed=35):
        self.trips[provider]['duration_ratio'] = self.trips[provider]['duration'] / self.trips[provider]['otp_duration']
        self.trips[provider] = self.trips[provider].loc[(self.trips[provider]['duration_ratio'] < max_duration_ratio)]
        print('remove non-direct trips done', provider, self.trips[provider].shape)
        self.trips[provider]['speed'] = (self.trips[provider]['otp_length'] / 1000) / (self.trips[provider]['duration'] / 60)  # km/h
        self.trips[provider] = self.trips[provider].loc[self.trips[provider]['speed'] < max_speed]
        print('remove too fast self.trips done', provider, self.trips[provider].shape)

    def distance(self, from_provider, to_provider, station_column, se):
        stations = self.trips[to_provider].drop_duplicates('{}_{}'.format(se, station_column))[
            ['{}_{}'.format(se, station_column), '{}_km_lon'.format(se), '{}_km_lat'.format(se)]].to_dict(
            orient='records')
        print(stations)
        id_stations = {i: x['{}_{}'.format(se, station_column)] for i, x in enumerate(stations)}
        kdt_other = KDTree(
        [
            [
                x['{}_km_lon'.format(se)],
                x['{}_km_lat'.format(se)]
            ] for x in stations
        ])
        closest_other_end = self.trips[from_provider].apply(
            lambda row: kdt_other.query([row['end_km_lon'], row['end_km_lat']]), axis=1)
        closest_other_start = self.trips[from_provider].apply(
            lambda row: kdt_other.query([row['start_km_lon'], row['start_km_lat']]), axis=1)
        self.trips[from_provider]['end_dist_to_{}'.format(to_provider)] = closest_other_end.apply(
            lambda x: x[0])
        self.trips[from_provider]['end_place_{}'.format(to_provider)] = closest_other_end.apply(
            lambda x: id_stations[x[1]])
        self.trips[from_provider]['start_dist_to_{}'.format(to_provider)] = closest_other_start.apply(
            lambda x: x[0])
        self.trips[from_provider]['start_place_{}'.format(to_provider)] = closest_other_start.apply(
            lambda x: id_stations[x[1]])
        
    def angles(self, provider):
        for ll in ['lat', 'lon']:
            self.trips[provider][
                'distance_{}'.format(ll)] = self.trips[provider][
                 'end_km_{}'.format(ll)] - self.trips[provider]['start_km_{}'.format(ll)]
        self.trips[provider]['angle'] = self.trips[provider].apply(
            lambda row: np.angle([complex(row['distance_lon'], row['distance_lat'])], deg=True)[0],
            axis=1)

    def angle_to_provider(self, from_provider, to_provider, id_column, se):
        veturilo_coordinates = {}
        for veturilo_station in self.trips[from_provider]['{}_place_{}'.format(se, to_provider)].unique():
            if self.trips[to_provider].loc[
                self.trips[to_provider]['{}_{}'.format(se, id_column)] == veturilo_station].shape[0] == 0:
                continue
            veturilo_coordinates[veturilo_station] = self.trips[to_provider].loc[
                self.trips[to_provider]['{}_{}'.format(se, id_column)] == veturilo_station].iloc[0][[
                '{}_km_lat'.format(se), '{}_km_lon'.format(se)]].to_dict()
            print(veturilo_coordinates[veturilo_station])
        #self.trips[from_provider]['start_{}_km_lat'.format(to_provider)],\
        #self.trips[from_provider]['start_{}_km_lon'.format(to_provider)] = \
        #    [self.trips[from_provider]['start_place_{}'.format(to_provider)].map(
        #        veturilo_coordinates
        #    ).apply(lambda x: x[i]) for i in ['{}_km_lat'.format(se), '{}_km_lon'.format(se)]]
        print(self.trips[from_provider][
            'start_place_{}'.format(to_provider)].map(veturilo_coordinates))
        for ll in ['lat', 'lon']:
            self.trips[from_provider]['start_{}_km_{}'.format(to_provider, ll)] = self.trips[from_provider][
                'start_place_{}'.format(to_provider)].apply(lambda x: veturilo_coordinates.get(
                x, {'start_km_{}'.format(ll): None})['start_km_{}'.format(ll)])
        for ll in ['lat', 'lon']:
            self.trips[from_provider]['distance_{}_{}'.format(to_provider, ll)
            ] = self.trips[from_provider]['start_{}_km_{}'.format(to_provider, ll)
                            ] - self.trips[from_provider]['start_km_{}'.format(ll)]
        self.trips[from_provider]['{}_angle'.format(to_provider)] = self.trips[from_provider].apply(
            lambda row: np.angle([complex(row['distance_{}_lon'.format(to_provider)],
                                          row['distance_{}_lat'.format(to_provider)])],
                                 deg=True)[0], axis=1)

    def pt_distances(self, provider, se, means, kdt: KDTree, dist_limit=70):
        self.trips[provider]['{}_{}_dist'.format(se, means)] = self.trips[provider].apply(
            lambda row: kdt.query([row['{}_km_lon'.format(se)],
                                   row['{}_km_lat'.format(se)]])[0], axis=1)
        self.trips[provider]['{}_{}_bool'.format(se, means)
        ] = self.trips[provider]['{}_{}_dist'.format(se, means)] < dist_limit

    @staticmethod
    def jitter(x, r=1):
        angle = random.uniform(0, 2*math.pi)
        return (x[0] + r*math.cos(angle), x[1] + r*math.sin(angle))

    @staticmethod
    def flatten(x):
        return [a for b in x for a in b]

    @staticmethod
    def get_coords(x):
        try:
            return [[j.coords[0] for j in i.geoms] for i in x]
        except Exception as ex:
            return [([i.coords[0]] if type(i) == Point else [j.coords[0] for j in i.geoms]) for i in x]

    def jitter_col(self, provider, col, r=1):
        self.trips[provider][col] = self.trips[provider][col].apply(lambda x: self.jitter(x, r) if x else x)

    def access(self, from_provider, to_provider, filename, column, random_access=False):
        if not os.path.exists(filename):
            if column is None:
                access_col = '{}_coord_access'.format(to_provider)
                self.trips[from_provider][access_col] = list(zip(
                    self.trips[from_provider]['start_{}_km_lon'.format(to_provider)].values.tolist(),
                    self.trips[from_provider]['start_{}_km_lat'.format(to_provider)].values.tolist()
                ))
                column = access_col
            print(column)
            if random_access:
                random_points = pd.read_pickle(os.path.join(
                    '..', 'out', 'suspects_intersect_all_{}.pickle'.format(self.filesuffix)))
                self.trips[from_provider][column] = [(random.choice(self.flatten(self.get_coords(x)))
                    if x else []) for x in random_points.values()]
                self.jitter_col(from_provider, column)
                self.save('jittered')
            linestrings = pd.read_pickle(os.path.join('..', 'out', 'linestrings_{}.pickle'.format(self.filesuffix)))
            print(datetime.now())
            cross_counts = []
            print(self.trips[from_provider][column])
            for i, trip in self.trips[from_provider].iterrows():
                print('i', i, datetime.now(), self.trips[from_provider].index.max())
                p1 = Point(trip['start_km_lon'], trip['start_km_lat'])
                vet1 = Point(*trip[column])
                if not vet1 or np.isnan(trip[column][0]):
                    cross_counts.append(0)
                    continue
                vet_line = LineString([p1, vet1])
                single_count = 0
                for l in linestrings:
                    single_count += vet_line.intersects(l)
                print(ttr(*p1.coords[0])[::-1], ttr(*vet1.coords[0])[::-1], single_count)
                cross_counts.append(single_count)
            pickle.dump(cross_counts, open(filename, 'wb'))
            print(datetime.now())

        self.trips[from_provider]['cross_counts_{}'.format(to_provider)] = pd.read_pickle(filename)

    def save(self, suffix):
        pickle.dump(self.trips, open(os.path.join(
            '..', 'out', 'all_trips_{}_{}.pickle'.format(suffix, self.filesuffix)), 'wb'))


class Analysis(Transport):
    def __init__(self, trips, stays, relocations, filesuffix, img_dir='./'):
        Transport.__init__(self, trips, filesuffix)
        self.stays = stays
        self.relocations = relocations
        self.img_dir = img_dir

    def plot_kde(self, se, is_all, col):
        col_name = col.replace('_', ' ').capitalize()
        if col == 'otp_length':
            col_name = 'OTP distance'
        fig, ax = plt.subplots(figsize=(10,6))
        if is_all:
            for k, v in self.trips.items():
                sns.kdeplot(
                    v[col], label='{} trips'.format(k), gridsize=300, cut=0, ax=ax)
            # ax.set_title('KDE plot of all trips\' {}'.format(col_name))
        else:
            for k, v in self.trips.items():
                for group in self.groups:
                    sns.kdeplot(v.loc[v['{}_{}'.format(se, group)] == True][col],
                                gridsize=300, cut=0, label='{} trips {}ing in {}'.format(
                                     k, se, group.replace('central', 'centre')), ax=ax)
            # ax.set_title('KDE plot of {}, trips split by where they {}'.format(col_name, se))
        if col == 'duration':
            ax.set_xlim((0, 30))
        elif col == 'otp_length':
            ax.set_xlim((0, 7500))
        plt.ylabel('Density')
        unit = ''
        if col == 'speed':
            unit = ' [km/h]'
        elif col == 'duration':
            unit = ' [minutes]'
        elif col == 'otp_length':
            unit = ' [meters]'
        plt.xlabel(col_name + unit)
        plt.legend()
        plt.tight_layout()
        plt.savefig(os.path.join(self.img_dir, '{}xx{}xxall{}.eps'.format(col, se, is_all)))
        plt.close(fig)

    def ks_test(self, providers, groups, col):
        assert len(providers) == 2 and len(groups) == 2, 'Two-sample test requires precisely two providers and groups.'

        s = [self.trips[providers[0]], self.trips[providers[1]]]
        for i, group in enumerate(groups):
            if group is not None:
                assert all([x in group.keys() for x in ['se', 'group', 'bool']]),\
					'Not enough required parameters in group {} dict'.format(i)
                # print('{}_{}'.format(group['se'], group['group']), group['bool'], s[i].columns.tolist())
                s[i] = s[i].loc[s[i]['{}_{}'.format(group['se'], group['group'])] == group['bool']]

        return stats.ks_2samp(s[0][col].values, s[1][col].values)
    
    def access(self, from_provider, to_provider, filename, column):
        if not os.path.exists(filename):
            linestrings = pd.read_pickle(os.path.join('..', 'out', 'linestrings_{}.pickle'.format(self.filesuffix)))
            print(datetime.now())
            cross_counts = []
            for i, trip in self.trips[from_provider].iterrows():
                p1 = Point(trip['start_km_lon'], trip['start_km_lat'])
                vet1 = Point(*trip[column])
                if not vet1:
                    cross_counts.append(0)
                    continue

                vet_line = LineString([p1, vet1])
                single_count = 0
                for l in linestrings:
                    single_count += vet_line.intersects(l)
                cross_counts.append(single_count)
            pickle.dump(cross_counts, open(filename, 'wb'))
            print(datetime.now())

        self.trips[from_provider]['cross_counts_{}'.format(to_provider)] = pd.read_pickle(filename)

    def compute_distances(self, from_provider, to_provider, se):
        for ll in ['lat', 'lon']:
            self.trips[from_provider]['distance_{}_{}'.format(to_provider, ll)] = self.trips[from_provider][
            '{}_{}_km_{}'.format('end', to_provider, ll)] - self.trips[from_provider][
            '{}_{}_km_{}'.format('start', to_provider, ll)]

    def plot_angles(self, from_provider, to_provider, limit_type='median'):
        fig, ax = plt.subplots(figsize=(10,6))
        limit = 0
        if limit_type == 'median':
            limit = self.trips[from_provider]['start_dist_to_{}'.format(to_provider)].median()
        elif limit_type == 'mean':
            limit = self.trips[from_provider]['start_dist_to_{}'.format(to_provider)].mean()
        ag = self.trips[from_provider].loc[
                     self.trips[from_provider]['start_dist_to_{}'.format(to_provider)] > limit,
                     'angle_difference']
        print(limit_type, (ag // 10).value_counts())
        ax.hist(ag, bins=18)
        ax.set_xlabel('Angle between lines representing \nthe trip and the hypothetical\nwalk to Veturilo')
        if limit_type == 'median':
            ax.set_ylabel('Number of trips,\nat least median length')
        elif limit_type == 'mean':
            ax.set_ylabel('Number of trips,\nat least mean length')
        else:
            ax.set_ylabel('Number of trips')
        plt.tight_layout()
        plt.savefig(os.path.join(self.img_dir, 'hist_angle_{}.eps'.format(limit_type)))
        plt.close()

    def relocation_map(self, provider, sample_size=1000):
        m = folium.Map(location=[52.23, 21])
        for i, row in self.relocations[provider].sample(sample_size).iterrows():
            folium.CircleMarker(location=[float(row['finish_stay_lat']), float(row['finish_stay_lon'])],
                                color='green', fill=True, fill_opacity=0.1, opacity=0.1, radius=4).add_to(m)
        m.save(os.path.join(self.img_dir, 'relocations.html'))

    def near_hubs(self, provider, hubs, max_distance):
        for place, latlon in hubs.items():
            self.relocations[provider]['dist_{}'.format(place)] = self.relocations[provider].apply(
                lambda row: ((row['finish_stay_km_lon'] - latlon[0]) ** 2 + (
                        row['finish_stay_km_lat'] - latlon[1]) ** 2) ** (1 / 2), axis=1)
            print(place,
                  100 * self.relocations[provider].loc[self.relocations[provider]['dist_{}'.format(place)] < max_distance
                  ].shape[0] / self.relocations[provider].shape[0])

    def percentages_groups(self, provider, pt, group=None, se='start'):
        trips_sh = self.trips[provider].copy(deep=True)
        if group is not None:
            trips_sh = trips_sh.loc[trips_sh['{}_{}'.format(se, group)] == True]
        print(provider, pt, group, se,
              trips_sh['start_{}_bool'.format(pt)].sum() / trips_sh.shape[0],
              trips_sh['end_{}_bool'.format(pt)].sum() / trips_sh.shape[0],
              trips_sh.shape[0])

    def percentages_group(self, provider, pt, group=None, groupby_column='time_of_day', se='start'):
        trips_sh = self.trips[provider].copy(deep=True)
        trips_sh = trips_sh.loc[trips_sh['{}'.format(se, group)] == True]
        for k, df_metro in trips_sh[provider].loc[trips_sh[provider]['{}'.format(se, group)] == True].groupby(
                'time_of_day'):
            print(k, df_metro['start_tram_bool'].value_counts(normalize=True))

    def transfers(self, provider, min_time=0, max_time=np.inf):
        # print(self.trips[provider].keys(), self.trips[provider]['duration'].value_counts())
        trips_sh = self.trips[provider].loc[
            (self.trips[provider]['duration'] >= min_time) & (self.trips[provider]['duration'] < max_time)]
        print(provider, min_time, max_time, (trips_sh['transit_transfers'] >= 1).sum() / trips_sh.shape[0])

