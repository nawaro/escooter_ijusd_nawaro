import pandas as pd
import xml.etree.cElementTree as ET
import os
from datetime import datetime
import pickle

username = 'anonymised'
dir_name = os.path.join('/home', username, 'Documents', 'mobility_scraping')
xml_dir_name = os.path.join(dir_name, 'veturilo_date')
save_dir = os.path.join('..', 'out', 'veturilo_trips')
bikes_traveling = {}
finished_trips = []
files = []
for date in sorted(os.listdir(xml_dir_name)):
    # if date.endswith('tar.xz') or date.startswith('2020') or date[:10] > '2019-12-03' or date[:10] < '2019-09-29':
    #    continue
    if date.endswith('tar.xz') or date.startswith('2019') or (
        date[:10] > '2019-12-03' and date[:10] < '2020-08-04') or date[:10] > '2020-10-07':
        continue
    print(date, 'include', len(files))
    files.extend([os.path.join(date, x) for x in sorted(os.listdir(os.path.join(xml_dir_name, date)))])
print(len(files))
# files = [x for x in files if x[-6] in ['0', '3']]  # 0th or 30th second
print(len(files))


for filenames in list(zip(files[:-1], files[1:], files[30:])):
    # print(filenames)
    try:
        old = ET.parse(os.path.join(xml_dir_name, filenames[0]))
        new = ET.parse(os.path.join(xml_dir_name, filenames[1]))
        future = ET.parse(os.path.join(xml_dir_name, filenames[2]))
    except ET.ParseError:
        print('Valid XML unavailable')
    start_time = pd.to_datetime(filenames[0].split('.')[0], format='%Y-%m-%d/%H-%M-%S')
    end_time = pd.to_datetime(filenames[1].split('.')[0], format='%Y-%m-%d/%H-%M-%S')
    if start_time.date() != end_time.date():
        print(start_time.date(), end_time.date(), datetime.now())

    old_bike_counts = {place.attrib['uid']: place.attrib['bikes'] for place in old.findall('//place')}
    future_bike_counts = {place.attrib['uid']: place.attrib['bikes'] for place in future.findall('//place')}

    stations_under_relocation = []
    bikes = {}
    for place in old.findall('//place'):
        uid = place.attrib['uid']
        if (int(old_bike_counts.get(uid, 0)) - int(future_bike_counts.get(uid, 0)) > 5) and (
            int(place.attrib['bikes']) - int(place.attrib['bike_racks']) >= -2):
            # print('=== relocated', uid, place.attrib['name'], old_bike_counts[uid], future_bike_counts[uid],
            #       place.attrib['bikes'], place.attrib['bike_racks'])
            stations_under_relocation.append(uid)
        place_bikes = place.findall('./bike')
        for bike in place_bikes:
            bikes[bike.attrib['number']] = {**bike.attrib, **{'place_' + k: v for k, v in place.attrib.items()}}

    if stations_under_relocation:
        print('\n===\nunder relocation\n===\n', stations_under_relocation)

    bikes_next = {}
    for place in new.findall('//place'):
        for bike in place.findall('./bike'):
            bikes_next[bike.attrib['number']] = {**bike.attrib, **{'place_' + k: v for k, v in place.attrib.items()}}

    bikes_start = {k: {**v, 'time': start_time, 'relocated': v['place_uid'] in stations_under_relocation}
                   for k, v in bikes.items() if k not in bikes_next.keys()}
    if len(set(bikes_traveling.keys()) & set(bikes_start.keys())) > 0:
        print('\n\n\n===\n\n\n', set(bikes_traveling.keys()) & set(bikes_start.keys()), '\n\n\n===\n\n\n')
    bikes_traveling = {**bikes_traveling, **bikes_start}

    bikes_end = {k: {**v, 'time': start_time} for k, v in bikes_next.items()
                 if k not in bikes.keys()}
    for k, v in bikes_end.items():
        if k in bikes_traveling.keys():
            trip = {**{'start_' + k1: v1 for k1, v1 in bikes_traveling[k].items()},
                    **{'end_' + k1: v1 for k1, v1 in v.items()}}
            finished_trips.append(trip)
    bikes_traveling = {k: v for k, v in bikes_traveling.items() if k not in bikes_end.keys()}

    if start_time.date() != end_time.date():
        pickle.dump(finished_trips, open(os.path.join(save_dir,
                                                      'veturilo_' + str(start_time.date()) + '.pickle'), 'wb'))
        finished_trips = []
    print(end_time, len(bikes_traveling), len(finished_trips))
