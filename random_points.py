import pandas as pd
import osmnx as ox
import networkx as nx
import geopandas as gp
import os
import random
import math
import pickle
from datetime import datetime
from shapely.geometry import LineString
from shapely.geometry import Point

# coordinates
from transformers import tt, ttr

# spatial
from geopy.distance import distance

filesuffix = 'allveturilo'

wg = ox.graph.graph_from_xml(
    os.path.join('..', 'in', 'Warsaw_streets.osm'))
lines = []
for i, edge in enumerate(list(wg.edges)):
    start, end, x = edge
    lines.append({'start': (wg.nodes[start]['x'],
                    wg.nodes[start]['y']),
                 'end': (wg.nodes[end]['x'],
                    wg.nodes[end]['y'])})
swietokrzyska = [52.235248, 21.008442]
new_lines = []
for i, line in enumerate(lines):
    if distance(line['start'][::-1], swietokrzyska).km < 12.5:
        new_lines.append(line)
new_lines_km = []
for line in new_lines:
    new_lines_km.append({'start': tt(*line['start']),
                        'end': tt(*line['end'])})
linestrings = []
for point in new_lines_km:
    linestrings.append(LineString([point['start'], point['end']]))
pickle.dump(linestrings, open(os.path.join('..', 'out', 'linestrings_{}.pickle'.format(filesuffix)), 'wb'))
linestrings = pd.read_pickle(os.path.join('..', 'out', 'linestrings_{}.pickle'.format(filesuffix)))

trips = pd.read_pickle(os.path.join('..', 'out', 'all_trips_mod_{}.pickle'.format(filesuffix)))

#trips['hive']['start_place_veturilo_coord'] = trips[
#    'hive']['start_place_veturilo'].map(
#    trips['veturilo'][
#    ['end_place_number', 'end_lat', 'end_lon']].set_index(
#    'end_place_number').to_dict()['end_{}'.format(name)])

for count, name in [(0, 'lon'), (1, 'lat')]:
    #trips['hive']['start_place_veturilo_{}'.format(name)] = trips[
    #    'hive']['start_place_veturilo_coord'].apply(
    #    lambda x: float(x.split(', ')[count]))
    trips['hive']['start_place_veturilo_{}'.format(name)] = trips[
        'hive']['start_place_veturilo'].map(
        trips['veturilo'][
        ['end_place_number', 'end_lat', 'end_lon']].set_index(
       'end_place_number').to_dict()['end_{}'.format(name)])

print(datetime.now())
suspects = {}
for i, row in trips['hive'].iterrows():
    point_suspects = []
    p = Point(row['start_km_lon'], row['start_km_lat'])
    print('trip', p)
    c = p.buffer(row['start_dist_to_veturilo']).boundary
    for l in linestrings:
        if c.intersects(l):
            point_suspects.append(c.intersection(l))
    suspects[i] = point_suspects

print(datetime.now())
print(suspects)
pickle.dump(suspects, open(os.path.join('..', 'out', 'suspects_intersect_all_{}.pickle'.format(filesuffix)), 'wb'))
