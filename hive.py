import os
from collections import defaultdict
import json
import pandas as pd
import pickle

username = 'anonymised'
tar_dir_name = os.path.join('/home', username, 'Documents', 'mobility_scraping', 'hive_tars')
dir_name = os.path.join('/home', username, 'Documents', 'mobility_scraping', 'hive_date')

# for i in os.listdir(tar_dir_name):
#     os.system('tar -xzvf {} -C {}'.format(os.path.join(tar_dir_name, i), dir_name))

positions = defaultdict(lambda: [])
files = []
for date in sorted(os.listdir(dir_name)):
    print(date)
    if date.endswith('tar.xz') or date.startswith('2019') or (
            date[:10] > '2019-12-03' and date[:10] < '2020-08-04') or date[:10] > '2020-10-07':
        continue
    files.extend([os.path.join(dir_name, date, x) for x in sorted(os.listdir(os.path.join(dir_name, date)))])

print(files)

for old_name, new_name in zip(files, files[1:]):
    try:
        old = pd.read_json(old_name)
        if old.shape[0] == 0:
            continue
        old['date'] = old_name.split('/')[-2]
        old['time'] = old_name.split('/')[-1].replace(".json", '')
        old = old.set_index('carId')[['lat', 'lon', 'time', 'date', 'fuelLevel']].to_dict(orient='index')
        new = pd.read_json(new_name)
        if new.shape[0] == 0:
            continue
        new['date'] = new_name.split('/')[-2]
        new['time'] = new_name.split('/')[-1].replace('.json', '')
        new = new.set_index('carId')[['lat', 'lon', 'time', 'date', 'fuelLevel']].to_dict(orient='index')
    except ValueError:
        print('error', old_name, new_name)
        continue

    print(old_name, new_name, len(positions))

    for ids in old.keys():
        if ids not in new.keys():
            positions[ids].append({'disappearing': True, 'time': old[ids]['time'], 'date': old[ids]['date'],
                                   'fuel_level': old[ids]['fuelLevel'],
                                   'lat': old[ids]['lat'], 'lon': old[ids]['lon']})
    for ids in new.keys():
        if ids not in old.keys():
            positions[ids].append({'appearing': True, 'time': new[ids]['time'], 'date': new[ids]['date'],
                                   'fuel_level': new[ids]['fuelLevel'],
                                   'lat': new[ids]['lat'], 'lon': new[ids]['lon']})

trips = {}
for k, v in positions.items():
    for i in range(len(v)-1):
        if v[i].get('disappearing') and v[i+1].get('appearing'):
            if v[i+1]['date'] not in trips.keys():
                trips[v[i + 1]['date']] = []
            trips[v[i+1]['date']].append({
                'id': k,
                'start_lat': v[i]['lat'], 'start_lon': v[i]['lon'],
                'end_lat': v[i+1]['lat'], 'end_lon': v[i+1]['lon'],
                'start_date': v[i]['date'], 'end_date': v[i+1]['date'],
                'start_time': v[i]['time'], 'end_time': v[i+1]['time'],
                'start_fuel': v[i]['fuel_level'], 'end_fuel': v[i+1]['fuel_level'],
                'duration': (pd.to_datetime(v[i+1]['time'].replace('-', ':')) - pd.to_datetime(
                    v[i]['time'].replace('-', ':'))).seconds - 30
            })

# pickle.dump(trips, open(os.path.join('..', 'out', 'hive_trips.json'), 'wb'))
for k, v in trips.items():
    pickle.dump(v, open(os.path.join('..', 'out', 'hive_trips', k + '.pickle'), 'wb'))
