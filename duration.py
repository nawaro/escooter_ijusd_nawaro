import pandas as pd
import numpy as np
import json
import pickle
from scipy.spatial import KDTree
from geopy.distance import distance
from transformers import tt, ttr
from full_functions import Modify

filesuffix = 'allveturilo'

subway_entrances = json.load(open('../in/subway_entrances.geojson', 'r'))['features']
tram_stops = json.load(open('../in/tram_stops.geojson', 'r'))['features']
kdt_subway = KDTree([tt(*x['geometry']['coordinates']) for x in subway_entrances])
kdt_tram = KDTree([tt(*x['geometry']['coordinates']) for x in tram_stops])

trips = {'veturilo': pd.concat(
#    [pd.DataFrame(pd.read_pickle('../out/veturilo_trips/veturilo_2019-10-{}.pickle'.format(x)))
#     for x in range(12, 32)] +
#    [pd.DataFrame(pd.read_pickle('../out/veturilo_trips/veturilo_2019-11-{}.pickle'.format(str(x).zfill(2))))
#     for x in range(1, 31)] +
             [pd.DataFrame(pd.read_pickle('../out/veturilo_trips/veturilo_2020-08-{}.pickle'.format(str(x).zfill(2))))
               for x in range(4, 32)] +
              [pd.DataFrame(pd.read_pickle('../out/veturilo_trips/veturilo_2020-09-{}.pickle'.format(str(x).zfill(2))))
               for x in range(1, 9)] +
              [pd.DataFrame(pd.read_pickle('../out/veturilo_trips/veturilo_2020-09-{}.pickle'.format(str(x).zfill(2))))
               for x in range(12, 31)] +
              [pd.DataFrame(pd.read_pickle('../out/veturilo_trips/veturilo_2020-10-{}.pickle'.format(str(x).zfill(2))))
               for x in range(1, 7)]
).reset_index(),
         'hive': pd.concat(
#             [pd.DataFrame(pd.read_pickle('../out/hive_trips/2019-10-{}.pickle'.format(x)))
#              for x in range(12, 18)] +
#             [pd.DataFrame(pd.read_pickle('../out/hive_trips/2019-10-{}.pickle'.format(x)))
#              for x in range(24, 32)] +
#             [pd.DataFrame(pd.read_pickle('../out/hive_trips/2019-11-{}.pickle'.format(str(x).zfill(2))))
#              for x in range(1, 31)] +
             [pd.DataFrame(pd.read_pickle('../out/hive_trips/2020-08-{}.pickle'.format(str(x).zfill(2))))
              for x in range(4, 32)] +
             [pd.DataFrame(pd.read_pickle('../out/hive_trips/2020-09-{}.pickle'.format(str(x).zfill(2))))
              for x in range(1, 9)] +
             [pd.DataFrame(pd.read_pickle('../out/hive_trips/2020-09-{}.pickle'.format(str(x).zfill(2))))
              for x in range(12, 31)] +
             [pd.DataFrame(pd.read_pickle('../out/hive_trips/2020-10-{}.pickle'.format(str(x).zfill(2))))
              for x in range(1, 7)]
         ).reset_index()}

# particular to one provider
print(trips['hive']['start_date'] + 'T' + trips['hive']['start_time'].str.replace(
    '-', ':') + '+00:00', trips['hive']['start_time'])

# rename columns
trips['veturilo'].rename({'start_place_lng': 'start_lon', 'start_place_lat': 'start_lat',
                          'end_place_lng': 'end_lon', 'end_place_lat': 'end_lat'},
                         axis='columns', inplace=True)
# remove relocation trips
print('remove relocation', trips['veturilo'].shape)
trips['veturilo'] = trips['veturilo'].loc[trips['veturilo']['start_relocated'] == False]
print('remove relocation done', trips['veturilo'].shape)


# prettier date
trips['hive']['start_time'] = (trips['hive']['start_date'] + 'T' + trips['hive']['start_time'].str.replace(
    '-', ':') + '+00:00').apply(pd.to_datetime)
print('start date', trips['hive'].head())

# durations in minutes
trips['hive']['duration'] /= 60
trips['veturilo']['duration'] = (trips['veturilo']['end_time'] - trips['veturilo']['start_time']).apply(
    lambda x: x.seconds) / 60

# price
trips['veturilo']['free'] = trips['veturilo']['duration'] < 20
trips['veturilo']['over_hour'] = trips['veturilo']['duration'] > 60
trips['hive']['price'] = np.ceil(trips['hive']['duration']) * 0.5 + 3

# remove Veturilko stations
vc = trips['veturilo']['end_place_number'].value_counts()
vc = vc[vc > 10].index
print('remove Veturilko stations', trips['veturilo'].shape)
trips['veturilo'] = trips['veturilo'].loc[trips['veturilo']['end_place_number'].isin(vc)]
print('remove Veturilko stations done', trips['veturilo'].shape)

all_sources = list(trips.keys())

md = Modify(trips, filesuffix)
# remove trips with same starting and ending points
print('remove roundtrips', trips['hive'].shape, trips['veturilo'].shape)
md.remove_roundtrips('hive', latlon=True)
md.remove_roundtrips('veturilo', latlon=False, column_suffix='place_name')
print('remove roundtrips done', trips['hive'].shape, trips['veturilo'].shape)

for k in all_sources:
    md.column_times(k)  # time of day
    print('before values', md.trips['veturilo'].head(), md.trips['hive'].head())
    md.otp_values(k)  # OTP basic data
    print('after values', md.trips['veturilo'].head(), md.trips['hive'].head())
    md.otp_alternative(k)  # OTP transit alterantive
    md.remove_suspects(k)  # remove erroneous trips
    for se in ['start', 'end']:
        md.km_coords(k, se)  # km coordinates
        md.pt_distances(k, se, 'tram', kdt_tram)
        md.pt_distances(k, se, 'metro', kdt_subway)
    md.centrality(k)
    md.angles(k)

md.distance('hive', 'veturilo', 'place_number', 'end')
md.angle_to_provider('hive', 'veturilo', 'place_number', 'start')
md.save('mod')

trips = pd.read_pickle('../out/all_trips_mod_{}.pickle'.format(filesuffix))
# Hive stays
stays = []
relocations = []
counter = 0
for ids, trips_id in trips['hive'].groupby('id'):
    for i, j in zip(trips_id.index, trips_id.index[1:]):
        print(i, j, trips_id.loc[i], trips_id.loc[j])
        fuel_difference = trips_id.loc[j, 'start_fuel'] - trips_id.loc[i, 'end_fuel']
        stay_duration = (((trips_id.loc[j, 'start_time'] - trips_id.loc[i, 'start_time']).seconds) / 60) - trips_id.loc[i, 'duration']
        stay_distance = distance(trips_id.loc[i, ['end_lat', 'end_lon']].values,
                                 trips_id.loc[j, ['start_lat', 'start_lon']].values).m
        stay_dict = {
                'id': trips_id.loc[i, 'id'], 'fuel_diff': fuel_difference,
                'stay_duration': stay_duration,
                'stay_distance': stay_distance,
                'begin_date': trips_id.loc[i, 'end_date'],
                'begin_time': trips_id.loc[i, 'end_time'],
                'finish_date': trips_id.loc[j, 'start_date'],
                'finish_time': trips_id.loc[j, 'start_time'],
                'begin_stay_km_lat': trips_id.loc[i, 'end_km_lat'],
                'begin_stay_km_lon': trips_id.loc[i, 'end_km_lon'],
                'begin_stay_lat': trips_id.loc[i, 'end_lat'],
                'begin_stay_lon': trips_id.loc[i, 'end_lon'],
                'finish_stay_km_lat': trips_id.loc[j, 'start_km_lat'],
                'finish_stay_km_lon': trips_id.loc[j, 'start_km_lon'],
                'finish_stay_lat': trips_id.loc[j, 'start_lat'],
                'finish_stay_lon': trips_id.loc[j, 'start_lon']
            }
        if stay_distance > 250 or (fuel_difference > 25 and stay_duration > 5):
            pass
        else:
            stays.append(stay_dict)
        if fuel_difference > 10 and stay_duration > 5:
            relocations.append(stay_dict)

st = pd.DataFrame(stays)
rl = pd.DataFrame(relocations)

pickle.dump(st, open('../out/hive_stays_{}.pickle'.format(filesuffix), 'wb'))
pickle.dump(rl, open('../out/hive_relocations_{}.pickle'.format(filesuffix), 'wb'))

